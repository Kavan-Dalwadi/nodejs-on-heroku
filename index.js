const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000

express()
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/pathA', (req, res) => res.render('pages/index'))
  .get('/pathA/lang-logo.png', (req, res) => res.render('./public/lang-logo.png'))
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))
